# Assignment 1.2 - Documentation

# Start
To start the program you have to navigate to the folder via terminal and execute the following command (the paths to the required data are included in the file itself):

`python shakespeare.py`

# Information
There is a list of stop words that affects the result. If this is not desired then please just empty the list in the code. Additionally there is an output file to better check the result.