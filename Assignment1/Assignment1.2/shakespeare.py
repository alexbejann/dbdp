#!/usr/bin/python3
import re
from os import walk
import string
from map_reduce_lib import *

def map_lines_to_words(key_value_item):
    """ Map function for the word count job.
    1. Extract the line number
    2. Clean the Line from the Number
    3. Output is [Key: Value], Value is reference on the shakespeare work
    """
    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))

    output = []

    line_number = line.split("\t")[0]
    line = re.sub(r'^[0-9]+[ \t]', '', line)

    if len(line) > 0:
        # Convert to lowercase, trim and remove punctuation.
        line = line.lower().strip()
        line = line.translate(str.maketrans(string.punctuation, ' ' * len(string.punctuation)))

        line_at_string = file+"@"+line_number

        # List with stopwords (low information words that should be removed from the string)
        STOP_WORDS = {'a', 'an', 'and', 'are', 'as', 'be', 'by', 'for', 'if', 'in', 'i', 's', 'you', 'my', 'not', 'me',
                      'his', 'her', 'he', 'she', 'd', 'll', 'is', 'it', 'of', 'or', 'py', 'rst', 'that', 'the', 'to',
                      'with'}

        # Split into words and add to output list
        for word in line.split():

            # Only if word is not in the stop word list, add to output
            if (word not in STOP_WORDS) & (not word.isdigit()):
                output.append( (word, line_at_string) )
    
    return output

def reduce_word_count(key_value_item):
    """ Reduce function for the word count job. 
    Converts partitioned data (key, [value]) to a summary of form (key, value).
    """
    word, occurances = key_value_item
    return (word, str(len(occurances)) + " (Total) | " + ",".join(occurances))

if __name__ == '__main__':

    filenames = next(walk("./invertedIndexInput/"), (None, None, []))[2]

    # Read data into separate lines
    file_contents = read_files(filenames)
    
    # Execute MapReduce job in parallel
    map_reduce = MapReduce(map_lines_to_words, reduce_word_count, 8)
    word_counts = map_reduce(file_contents, debug=True)

    # Order the results in reverse order
    word_counts = sorted(word_counts, key=lambda x: int(len(x[1].split(','))), reverse=True)
    top5 = word_counts[:5]

    with open('output.txt', 'w') as f:
        for line in word_counts:
            f.write(f"{line}\n")
    
    print('First 5 words with reference:')
    for word, count in top5:
        print('{}: {}'.format(word, count))