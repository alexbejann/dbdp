# Assignment 1.1 - Documentation

# Start the code
Navigate to this folder via terminal and run the following command for the deliveries:
(*info:* you can change the small dataset folder to large dataset)
- 1.1.1: `python ex_1_1_1.py ./data/dataset-small/playhistory.csv`
- 1.1.2: `python ex_1_1_2.py ./data/dataset-small/playhistory.csv ./data/dataset-small/people.csv`
- 1.1.3: `python ex_1_1_3.py ./data/dataset-small/playhistory.csv ./data/dataset-small/tracks.csv`
- 1.1.4: `python ex_1_1_3.py ./data/dataset-small/playhistory.csv ./data/dataset-small/tracks.csv ./data/dataset-small/people.csv`

You can also add the files as parameters. More in the next section.



# Files
We added the files as parameters and read for each subtask in the following order

| Assignment | Parameters ordering                                          |
| --- |--------------------------------------------------------------|
| 1.1.1 | playhistory.csv                                              |
| 1.1.2 | playhistory.csv, people.csv                                  |
| 1.1.3 | playhistory.csv, tracks.csv                                  |
| 1.1.4 | playhistory.csv, tracks.csv, people.csv |
## Answer 1.1.4 question

The reason for using a cascade of mappers and reducers is due to the fact that we cannot join on any column therfore like it was in the first subtasks. In the end we would be able to merge 
For instance we have the people csv which has the following columns' id, first_name, last_name, email, gender, country, dob
playhistory has track_id, user, datetime and tracks has track_id, artist, title, lengthSeconds
knowing this we can first map the playhistory and the tracks files and then use the user_id to merge it with the people's csv and obtain our desired result which would look like this: fname, (lname, artist, count)
