#!/usr/bin/python3
from datetime import datetime
import sys
import os.path
from map_reduce_lib import *


def map_1_1_3(key_value_item):
    song_title = ''
    artist_name = ''

    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))
    output = []

    line = line.split(',')
    # ignore first line of each file
    if line[0] == 'track_id':
        return
    # map tracks
    if len(line) == 4:
        track_id = line[0]
        artist_name = line[1]
        song_title = line[2]
        output.append((track_id, (song_title, artist_name, 0)))
    elif len(line) == 3: # map track history
        track_id = line[0]
        date_time_hour = datetime.strptime(line[2], '%Y-%m-%d %H:%M:%S').hour
        if 7 == date_time_hour < 8:
            output.append((track_id, (song_title, artist_name, 1)))
    return output

def reduce_1_1_3(key_value_item):
    """ Reduce function for the word count job.
    Converts partitioned data (key, [value]) to a summary of form (key, value).
    """
    track_id, tuple_value = key_value_item
    # https://stackoverflow.com/questions/5693559/zip-function-help-with-tuples
    unzipped_list = list(zip(*tuple_value))
    # get the title from the list of titles,
    # most of these items are empty strings and there is one song title
    song_title = max(unzipped_list[0])
    # get the artist name, most of the items in this list should be
    # empty strings and one artist name
    artist_name = max(unzipped_list[1])
    # sum everything from the count list
    count_sum = sum(unzipped_list[2])

    return (count_sum, (song_title, artist_name))


if __name__ == '__main__':
    # Parse command line arguments
    if len(sys.argv) == 1:
        print('Please provide a text-file that you want to perform the wordcount on as a command line argument.')
        sys.exit(-1)
    elif not os.path.isfile(sys.argv[1]):
        print('File `%s` not found.' % sys.argv[1])
        sys.exit(-1)

    # Read data into separate lines
    file_contents = read_files([sys.argv[1], sys.argv[2]])

    # Execute MapReduce job in parallel
    map_reduce = MapReduce(map_1_1_3, reduce_1_1_3, 40)
    most_played_songs = map_reduce(file_contents, debug=True)

    # Order the results in reverse order
    most_played_songs.reverse()
    top5 = most_played_songs[:5]

    print('Top 5 most played songs in hour of the day:')
    for most_played, title_artist in top5:
        song_title, artist = title_artist
        count = most_played
        # 'title, artist, times played'
        print('Title: {0}, artist: {1}, times played: {2}'.format(song_title, artist, count))