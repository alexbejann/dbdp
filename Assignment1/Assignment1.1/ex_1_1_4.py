#!/usr/bin/python3
from datetime import datetime
import sys
import os.path
from map_reduce_lib import *


def map_history_tracks(key_value_item):
    """ Maps all the tracks based on id to the tracks history to replace the id
    """
    artist_name = ''
    user_id = '0'

    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))
    output = []

    line = line.split(',')
    # ignore first line of each file
    if line[0] == 'track_id':
        return
    # map tracks
    if len(line) == 4:
        track_id = line[0]
        artist_name = line[1]
        output.append((track_id, (artist_name, user_id)))
    elif len(line) == 3: # map track history
        track_id = line[0]
        user_id = line[1]
        output.append((track_id, (artist_name, user_id)))
    return output

def reduce_history_tracks(key_value_item):
    """ Reduce function for the word count job.
    Converts partitioned data (key, [value]) to a summary of form (key, value).
    """
    track_id, tuple_value = key_value_item
    # https://stackoverflow.com/questions/5693559/zip-function-help-with-tuples
    unzipped_list = list(zip(*tuple_value))
    # get the artist name, most of the items in this list should be
    # empty strings and one artist name
    artist_name = max(unzipped_list[0])
    # get the list of users who listened to this song
    # to be used later for counting
    users = list(unzipped_list[1])

    return track_id, (artist_name, users)

def map_people(key_value_item):
    """ Maps all the results from the previous mapReduce and uses the people.csv to
    count
    """
    artist_name = ''
    first_name = ''
    last_name = ''

    track_id, line = key_value_item
    process_print('is processing `%s` from %s' % (line, track_id))
    output = []
    # map people
    # is the line is a tuple, result of the previous mapReduce
    if not type(line) is tuple:
        line = line.split(',')
        # ignore first line of each file
        if line[0] == 'id':
            return
        user_id = line[0]
        # get first and last name
        first_name = line[1]
        last_name = line[2]
        output.append((user_id, (first_name, last_name, artist_name, 0)))
        return output

    # map track history result from first job
    # get array of users that listened to this track
    artist_name, users_that_listened = line
    # loop through the array of users
    for user_id in users_that_listened:
        output.append((user_id, (first_name, last_name, artist_name, 1)))
    return output

def reduce_people(key_value_item):
    """ Reduce function for the word count job.
    Converts the result from the maper and uses a dict to get the count
    and the artists
    """
    user_id, tuple_value = key_value_item
    # https://stackoverflow.com/questions/5693559/zip-function-help-with-tuples
    unzipped_list = list(zip(*tuple_value))
    # get the "max" of each list to get the fname and lname
    fname = max(unzipped_list[0])
    lname = max(unzipped_list[1])
    # create a dict with counts and artists
    # https://stackoverflow.com/questions/23240969/python-count-repeated-elements-in-the-list
    d = {i: unzipped_list[2].count(i) for i in unzipped_list[2]}
    # pop empty strings
    d.pop('', None)
    # get keys and values to use later for max
    count = list(d.values())
    artists = list(d.keys())
    # get artist index
    artist_index = count.index(max(count))
    # get the artist from list and count and return
    return fname, (lname, artists[artist_index], count[artist_index])

if __name__ == '__main__':
    # Parse command line arguments
    if len(sys.argv) == 1:
        print('Please provide a text-file that you want to perform the wordcount on as a command line argument.')
        sys.exit(-1)
    elif not os.path.isfile(sys.argv[1]):
        print('File `%s` not found.' % sys.argv[1])
        sys.exit(-1)

    # Merge playhistory.csv with tracks.csv
    # should look like this
    map_reduce_history_tracks = MapReduce(map_history_tracks, reduce_history_tracks, 8)
    file_contents_tracks_history = read_files([sys.argv[1], sys.argv[2]])
    # track_id:TRAWXFZ128F146C080, (artist_name: 'Soulsister', users_listened: [0, '58', '48', '184', '225', '90',
    # '16', '59'])
    tracks_history_result = map_reduce_history_tracks(file_contents_tracks_history, debug=True, chunksize=1024)
    # result from previous reducer use it to get the number of times listened
    map_reduce_people = MapReduce(map_people, reduce_people, 8)
    # read people.csv file and do the final arrangements
    people_file_content = read_files([sys.argv[3]])
    merged = people_file_content + tracks_history_result
    final_result = map_reduce_people(merged, debug=True, chunksize=1024)

    print('Each user, the artist (s)he listen to most often:')
    for fname, tuple in final_result:
        lname, artist, count = tuple
        # FirstName, LastName, Artist, NrofTimes listened to that artist
        print('FirstName: {0}, LastName: {1},Artist: {2}, {3} number of times listened to'.format(fname, lname, artist, count))