#!/usr/bin/python3
from collections import defaultdict
from datetime import datetime
import sys
import os.path
from map_reduce_lib import *


def map_lines_to_datetime(key_value_item):
    """ Map each line from the file."""

    # default values which we are going to use in the reducer
    first_name = ''
    last_name = ''
    date_time_hour = '-1'
    count = 0

    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))
    output = []

    line = line.split(',')
    # ignore first line of each file
    if line[0] == 'track_id' or line[0] == 'id':
        return
    # map people
    if len(line) == 7:
        id = line[0]
        first_name = line[1]
        last_name = line[2]
    else: # map play history
        id = line[1]
        count = 1
        date_time_hour = datetime.strptime(line[2], '%Y-%m-%d %H:%M:%S').hour
    # set id as key and create tuple with fname, lname, date_time_hour and count
    output.append( (id, (first_name, last_name, date_time_hour, count) ) )
    return output

def reduce_times_played(key_value_item):
    """ Reduce function for times played job.
    Converts partitioned data (id, (first_name, last_name, date_time_hour, count) )
    to (fname, lname), (hours[hour_index], count[hour_index]).
    """
    # get id and tuple value created previously
    id, tuple_value = key_value_item
    # reference: https://stackoverflow.com/questions/5693559/zip-function-help-with-tuples
    # get separate list of, fname, lname, date_time_hour and count
    unzipped_list = list(zip(*tuple_value))
    # get the "max" of each list to get the fname and lname
    fname = max(unzipped_list[0])
    lname = max(unzipped_list[1])
    # dict https://www.geeksforgeeks.org/python-get-sum-of-tuples-having-same-first-value/
    # init dict
    d = {x:0 for x in unzipped_list[2]}
    # iterate over tuple value and set the count
    for _, _, hour, count in tuple_value:
        d[hour] += count
    # needed as i couldn't do max(d, key=d.get) to get the max
    # so i created 2 lists with the values and keys and searched for the max index
    count = list(d.values())
    hours = list(d.keys())
    # get max index
    hour_index = count.index(max(count))
    # return fname,lname as key tuple, created (hours, count) as tuples to be returned and displayed
    return ((fname, lname), (hours[hour_index], count[hour_index]))

if __name__ == '__main__':
    # Parse command line arguments
    if len(sys.argv) == 1:
        print('Please provide a text-file that you want to perform the wordcount on as a command line argument.')
        sys.exit(-1)
    elif not os.path.isfile(sys.argv[1]):
        print('File `%s` not found.' % sys.argv[1])
        sys.exit(-1)

    # Read data into separate lines
    file_contents = read_files([sys.argv[1], sys.argv[2]])

    # Execute MapReduce job in parallel
    map_reduce = MapReduce(map_lines_to_datetime, reduce_times_played, 40)

    played_songs = map_reduce(file_contents, debug=True)

    print('Number of times played')
    #FirstName, LastName, hourOfday, numberOfTimesListened to a song in that hour of the day
    for fullname, most_played in played_songs:
        fname, lname = fullname
        hour, count = most_played
        # 'FirstName, LastName, hourOfDay, numberOfTimesListened to a song in that hour of the day'
        print('{0}, {1}, {2} (hour), {3} (count) to a song in that hour of the day'.format(fname, lname, hour, count))
