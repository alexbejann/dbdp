#!/usr/bin/python3
from datetime import datetime
import sys
import os.path
import string
from map_reduce_lib import *


def map_lines_to_datetime(key_value_item):
    """ Map function for the word count job. 
    Splits line into words, removes low information words (i.e. stopwords) and outputs (key, 1).
    """
    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))
    output = []

    # Convert to lowercase, trim and remove punctuation.
    line = line.split(',')

    # get year-month-day
    year_month_day = line[2].split(' ')[0]
    year_month = year_month_day.split('-')

    # check if has the file a correct structure
    if len(line) != 3 or len(year_month) != 3:
        return output

    # get month-year
    date = datetime.strptime(year_month[0] + '-' + year_month[1], '%Y-%m')

    # append to output and format string with short name for month and year
    # e.g. 'Jun 2019'
    output.append(((line[0], '{0} {1}'.format(date.strftime('%b'), date.year)), 1))

    return output


def reduce_times_played(key_value_item):
    """ Reduce function for the word count job. 
    Converts partitioned data (key, [value]) to a summary of form (key, value).
    """
    line, occurances = key_value_item
    return (line, sum(occurances))

#Function for sort
def by_song_id(e):
    return e[0]

if __name__ == '__main__':
    # Parse command line arguments
    if len(sys.argv) == 1:
        print('Please provide a text-file that you want to perform the wordcount on as a command line argument.')
        sys.exit(-1)
    elif not os.path.isfile(sys.argv[1]):
        print('File `%s` not found.' % sys.argv[1])
        sys.exit(-1)

    # Read data into separate lines
    file_contents = read_files([sys.argv[1]])

    # Execute MapReduce job in parallel
    map_reduce = MapReduce(map_lines_to_datetime, reduce_times_played, 8)
    played_songs = map_reduce(file_contents, debug=True)

    # Order the results in reverse order
    played_songs.sort(key=by_song_id)

    print('Number of times played')
    for line, count in played_songs:
        print('Track ID: {0}, {1} played in {2}'.format(line[0], count, line[1]))
