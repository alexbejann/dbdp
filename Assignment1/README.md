# Assignment 1 - Map Reduce
-  **Daniel-Alexandru Bejan (474404)**
-  **Patrick Schaper (534366)**

The project structure is simple. We have created a separate folder for each job with the required data. This is because each job can be viewed independently from the others. The individual readme files in the folders contain the exact steps for execution or further information. Readme's:
## [Assignment 1.1](\Assignment1.1\README.md)

## [Assignment 1.2](\Assignment1.2\README.md)

## [Assignment 1.3](\Assignment1.3\README.md)