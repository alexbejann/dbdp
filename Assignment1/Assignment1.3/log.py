#!/usr/bin/python3

import re
from datetime import datetime
from os import walk
from collections import Counter

from map_reduce_lib import *


def map_lines_to_words(key_value_item):
    """ Map function for the word count job.
    Splits line into words, removes low information words (i.e. stopwords) and outputs (key, 1).
    """
    file, line = key_value_item
    process_print('is processing `%s` from %s' % (line, file))

    output = []

    # get ip address
    ip_address = line.split(" ")[0]

    #create the necessary timestemp
    timestemp = re.search(r'\[.*]', line).group() #Output: [15/Jul/2009:15:50:36 -0700]
    timestemp = timestemp[1:-1].split(" ")[0] #Output: 15/Jul/2009:15:50:36
    timestemp = datetime.strptime(timestemp, "%d/%b/%Y:%H:%M:%S") #Output: 2009-07-15 15:50:36
    timestemp = str(timestemp.year) + "-" + str(timestemp.month) #Output: 2009-07

    output.append((ip_address, timestemp))

    return output

def reduce_word_count(key_value_item):
    """ Reduce function for the word count job.
    Converts partitioned data (key, [value]) to a summary of form (key, value).
    Example:
        ('10.190.26.23', '12 (Total) | 2009-07: 4, 2009-08: 3, 2009-9: 5')
    """
    ip_address, occurances = key_value_item

    total_string = str(len(occurances)) + " (Total) | "

    dcounts = Counter(occurances)
    month_string = ', '.join(key + ": " + str(value) for key, value in dcounts.items())

    return (ip_address, total_string + month_string)

if __name__ == '__main__':

    filenames = next(walk("./data/"), (None, None, []))[2]

    # Read data into separate lines
    file_contents = read_files(filenames)

    # Execute MapReduce job in parallel
    map_reduce = MapReduce(map_lines_to_words, reduce_word_count, 8)
    ip_address_counts = map_reduce(file_contents, debug=True)

    # Order the results in reverse order
    ip_address_counts = sorted(ip_address_counts, key=lambda x: int(x[1].split(' ')[0]), reverse=True)
    top20 = ip_address_counts[:20]

    with open('output.txt', 'w') as f:
        for line in ip_address_counts:
            f.write(f"{line}\n")

    print('Top 20 ip-addresses with frequency:')
    for ip_address, count in top20:
        print('{}: {}'.format(ip_address, count))